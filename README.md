# Passenger Service System: Frontend.

<!-- short intro will there  -->

Passenger Service System (PSS) facilitates all interactions between airlines and their clients, starting from ticket booking to boarding. Its primary objective is to ensure efficient and smooth airline operations. A modern PSS is a complex system that integrates numerous tools and programs to automate various passenger-related tasks. PSS application is divided into multiple microservices with a single frontend application. It is built using the Single Page Application (SPA) template and its features are stateless.

## Table of Contents

- [BackGround](#background)
- [Getting Started](#getting-started)
  - [Install](#install)
  - [Usage](#usage)
- [Instruction for pushing changes](#instruction-for-pushing-changes)
- [Features](#features)
- [Technology Stack](#technology-stack)

<br/>

## Background

The main purpose of Passenger Service System (PSS) is to provide a simple interface by which tickets can be issued to the airline's customers..The Passenger Service System (PSS) has evolved through three generations. <br/>
In the first generation, legacy systems met basic industry requirements but lacked modern features and flexibility, limiting revenue opportunities for airlines.The second generation involved integrating new technologies with existing systems, creating a patchwork of components that lacked reliability and consistency.The third generation embraces a service-oriented approach, with modular architectures like service-oriented architecture (SOA) and microservices. This enables airlines to add, update, or replace components without disrupting the entire system, leading to greater flexibility and scalability.Adapting to the changing landscape of PSSs means leveraging modern technologies and architectural approaches like microservices. This empowers airlines to provide enhanced user experiences and meet the expectations of today's travelers. <br/>
There are several modules which are responsible for the PSS's primary daily functions.

**Major Modules:**

| Number | Module Name                               | Short Description                                                                                                                                                                                                                                                                             |
| :----: | ----------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|   1    | Central Reservation System                | The airline or central reservation system acts as a comprehensive database and performs functions like managing reservations, displaying flight information, generating PNRs, and issuing tickets while integrating with various passenger touchpoints.                                       |
|   2    | Inventory, Pricing and Admin Module (IPA) | The primary goal of this module is to determine the price of a ticket, manage seat availability in different cabins and fare groups, often integrated with the airline reservation system to ensure timely information exchange and updates, manage flights schedule.                         |
|   3    | Sales                                     | This module serves as a ticket purchasing platform for customers while also providing functionalities for customer management, cash-box management, as well as agent and corporate customer management.                                                                                       |
|   4    | Departure Control System (DCS)            | A departure control system (DCS) streamlines passenger operations at the airport, including check-in, baggage handling, boarding pass generation, security information sharing, load sheet generation, and interaction with the airline reservation system for passenger and booking updates. |
|   5    | Reporting Module                          | It acts as a common platform where reports are stored and managed.                                                                                                                                                                                                                            |

<br/>

## Getting Started

1. In the frontend segement, this project uses [Node](https://nodejs.org/en/blog/release/v20.1.0) and [YARN](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable). Fisrtly check wheather you have them locally installed.

   ```cmd
   node -v && yarn -v
   ```

   If it is not installed, install them first aacording to provided link.

2. Clone this repository ([pss-frontend-application](https://github.com/mdgiasuddin/pss-frontend-application)).
   ```cmd
   git clone https://github.com/mdgiasuddin/pss-frontend-application
   ```

<br/>

## Install

1. Enable legacy OpenSSL provider.

   On Unix-like (Linux, macOS, Git bash, etc.):

   ```bash
   export NODE_OPTIONS=--openssl-legacy-provider
   ```

   On Windows command prompt:

   ```bash
   set NODE_OPTIONS=--openssl-legacy-provider
   ```

   On PowerShell:

   ```bash
   $env:NODE_OPTIONS = "--openssl-legacy-provider"
   ```

   In the project directory, you can run:

2. Runs the app in the development mode

   ```bash
   yarn install && yarn start
   ```

   Open [http://localhost:3000](http://localhost:3000) to view it in your browser.The page will reload when you make changes.You may also see any lint errors in the console.

## Usage

1. Launches the test runner in the interactive watch mode.

   ```bash
   yarn test
   ```

   See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

2. Builds the app for production to the `build` folder.

   ```bash
   yarn run build
   ```

   It correctly bundles React in production mode and optimizes the build for the best performance.The build is minified and the filenames include the hashes.
   Your app is ready to be deployed!

3. If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time.

   ```bash
   yarn run eject
   ```

   **Note: this is a one-way operation. Once you `eject`, you can't go back!**
   This command will remove the single build dependency from your project.

4. If you face any problem about node version. You can use [nvm](https://github.com/coreybutler/nvm-windows/releases).

   ### Linux and MAC nvm Installation

   1. Run the nvm installer:

      ```shell
      url -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

      or

      wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
      ```

   2. Update your profile configuration:

      ```shell
      export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
      ```

   3. Reload the shell configuration:
      ```shell
      source ~/.bashrc
      ```
   4. Verify the installation by running.
      ```shell
      nvm -v
      ```

   Install node using nvm.

   ```shell
   nvm install 20.1.0
   ```

   <br/>

## Instruction for pushing changes

To make any changes or to update your changes to the code base, please follow the given instructions(The first three should be followed each time you are assigned a new task and need to edit the codebase):

1. Go to the master branch using `git checkout master` or `git switch master` on the terminal
2. Update the master branch using `git pull` on the terminal
3. create a new local branch using `git checkout -b <branchname>` or `git switch -c <branchname>`. Make sure that your branch name should be meaningful and it should give some idea about your assigned task.
4. Commit and push your changes.

   - stage your changes using this: `git add [filename1] [filename2]`
   - commit your changes using this: `git commit -m "commit message"`. Your commit message should indicate what you have included in this commit.
   - Then push your changes using: `git push -u origin <branchname>`. Your newly created branch will be automatically added into the remote repository.

5. Create a pull request
   - Go to the remote repository. Go to 'Pull Requests'.
   - After then, create a new pull request by hitting the button 'New Pull Request'.
   - Select your branch from the top-down list, and then click on 'Create pull request'.

When your pull request is approved, it will be merged.

<br/>

## Features

- Flight management
- GDS/Partners parameters
- Classes management
- Fares management
- Taxes and Penalties
- Booking
- Search based on different factors
- Customer creation
- CRM tracking
- Corporate and Agency creation
- Cashbox management

<br/>

## Technology Stack

Fronted Developmet will be done using these libraries.

- _Node 20.1.0_
- _YARN 1.22.19_
- _React 18.2.0_
- _React Momment 1.1.3_
- _TypeScript 4.1.2_
- _Styled-Components 6.0.0_
- _React-Redux 7.2.6_
- _Redux/toolkit 1.9.5_
- _Redux Thunk 2.4.2_
- _Redux-Persist 6.0.0_
- _React Router Dom 6.11.1_
- _ANT Design 4.22.4_
- _ANT Design [Template](https://github.com/altence/lightence-ant-design-react-template)_
- _Antd Table Saveas Excel 2.2.1_
- _Axios 0.24.0_
- _Formik 2.2.9_
- _Formik antd 2.0.4_
- _Framer Motion 10.12.16_

<br/>
