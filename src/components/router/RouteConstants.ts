
export const routeConstants = {
    DASHBOARD_ROUTE: "/",
    ADMIN_ROUTE: "admin",
    GROUP_ROUTE: "group",
    SCREENLIST_ROUTE: "screenlist",
    USER_ROLE_MAPPING_ROUTE: "user-role",
    GROUP_ROLE_MAPPING_ROUTE: "group-role",

    AUTH_ROUTE: "auth",
    LOGIN_ROUTE: "login",
    SIGNUP_ROUTE: "sign-up",
    LOCK_ROUTE: "lock",
    FORGOT_PASSWORD_ROUTE: "forgot-password",
    SECURITY_CODE_ROUTE: "security-code",
    NEW_PASSWORD_ROUTE: "new-password",
    LOGOUT_ROUTE: "/auth/logout"
}
