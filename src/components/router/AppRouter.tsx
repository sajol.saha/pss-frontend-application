import ForgotPasswordPage from '@app/pages/ForgotPasswordPage';
import LockPage from '@app/pages/LockPage';
import LoginPage from '@app/pages/LoginPage';
import NewPasswordPage from '@app/pages/NewPasswordPage';
import SecurityCodePage from '@app/pages/SecurityCodePage';
import SignUpPage from '@app/pages/SignUpPage';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

// no lazy loading for auth pages to avoid flickering
const AuthLayout = React.lazy(() => import('@app/components/layouts/AuthLayout/AuthLayout'));

import MainLayout from '@app/components/layouts/main/MainLayout/MainLayout';
import RequireAuth from '@app/components/router/RequireAuth';
import { withLoading } from '@app/hocs/withLoading.hoc';

import { routeConstants } from '@app/components/router/RouteConstants';

const {
  DASHBOARD_ROUTE,
  ADMIN_ROUTE, 
  GROUP_ROUTE, 
  SCREENLIST_ROUTE, 
  USER_ROLE_MAPPING_ROUTE,
  GROUP_ROLE_MAPPING_ROUTE,
  AUTH_ROUTE,
  LOGIN_ROUTE,
  SIGNUP_ROUTE,
  LOCK_ROUTE,
  FORGOT_PASSWORD_ROUTE,
  SECURITY_CODE_ROUTE,
  NEW_PASSWORD_ROUTE,
  LOGOUT_ROUTE
} = routeConstants;

const GroupPage = React.lazy(() => import('@app/GroupManagement/UserGroup/UserGroup'));
const ScreenListPage = React.lazy(() => import('@app/GroupManagement/Screenlist/ScreenList'));
const GroupRolePage = React.lazy(() => import('@app/services/inventory/pages/GroupRolePage'));
const UserRolePage = React.lazy(() => import('@app/systemManagement/roleManagement/AddUserRoleForm'));

const Logout = React.lazy(() => import('./Logout'));

const GroupRole = withLoading(GroupRolePage);
const UserRole = withLoading(UserRolePage);
const Group = withLoading(GroupPage);
const ScreenList = withLoading(ScreenListPage);

const AuthLayoutFallback = withLoading(AuthLayout);
const LogoutFallback = withLoading(Logout);

export const AppRouter: React.FC = () => {
  const protectedLayout = (
    <RequireAuth>
      <MainLayout />
    </RequireAuth>
  );

  return (
    <BrowserRouter>
      <Routes>
        <Route path={DASHBOARD_ROUTE} element={protectedLayout}>
          
          <Route path={ADMIN_ROUTE}>

            <Route path={USER_ROLE_MAPPING_ROUTE} element={<UserRole />} />
            <Route path={GROUP_ROLE_MAPPING_ROUTE} element={<GroupRole />} />
            <Route path={GROUP_ROUTE} element={<Group />} />
            <Route path={SCREENLIST_ROUTE} element={<ScreenList />} />

          </Route>

        </Route>  
        
        <Route path={AUTH_ROUTE} element={<AuthLayoutFallback />}>
          <Route path={LOGIN_ROUTE} element={<LoginPage />} />
          <Route path={SIGNUP_ROUTE} element={<SignUpPage />} />
          <Route
            path={LOCK_ROUTE}
            element={
              <RequireAuth>
                <LockPage />
              </RequireAuth>
            }
          />
          <Route path={FORGOT_PASSWORD_ROUTE} element={<ForgotPasswordPage />} />
          <Route path={SECURITY_CODE_ROUTE} element={<SecurityCodePage />} />
          <Route path={NEW_PASSWORD_ROUTE} element={<NewPasswordPage />} />
        </Route>

        <Route path={LOGOUT_ROUTE} element={<LogoutFallback />} />
        
      </Routes>
    </BrowserRouter>
  );
};
