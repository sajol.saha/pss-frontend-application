import React from 'react';
import { useTranslation } from 'react-i18next';
import * as S from './DoctorProfile.styles';
import { UserOutlined } from '@ant-design/icons';
import { BaseAvatar } from '../BaseAvatar/BaseAvatar';

interface DoctorProfileProps {
  avatar?: string;
  name?: string;
  speciality?: string | number;
  rating?: number;
}

export const DoctorProfile: React.FC<DoctorProfileProps> = ({ avatar, name }) => {
  const { t } = useTranslation();

  return (
    <S.Profile>
      <BaseAvatar size="large" src={avatar} icon={<UserOutlined />} alt="Doctor avatar" shape="square" />
      <div>
        <S.Info>
          <S.Title>{t('common.doctor')}</S.Title>
          <S.Text>{name}</S.Text>
        </S.Info>
        <S.Info>
          <S.Title>{t('common.specifity')}</S.Title>
          {/*<S.Text>{t(`common.${specifity}`)}</S.Text>*/}
        </S.Info>
      </div>
    </S.Profile>
  );
};
