import React from 'react';
import * as S from './References.styles';
import { FacebookOutlined, LinkedinOutlined } from '@ant-design/icons';

export const References: React.FC = () => {
  return (
    <S.ReferencesWrapper>
      <S.Text>V 0.0.1</S.Text>
      <S.Text>
        Developed & Maintained By{' '}
        <a href="https://digigate360.com" target="_blank" rel="noreferrer">
          TechnoNext Ltd{' '}
        </a>
      </S.Text>
      <S.Icons>
        <a href="https://www.facebook.com/profile.php?id=100090609130913" target="_blank" rel="noreferrer">
          <FacebookOutlined />
        </a>
        <a href="https://www.linkedin.com/company/technonext/" target="_blank" rel="noreferrer">
          <LinkedinOutlined />
        </a>
      </S.Icons>
    </S.ReferencesWrapper>
  );
};
