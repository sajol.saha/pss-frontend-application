import React from 'react';
import { BaseSelect, BaseSelectProps } from '../BaseSelect/BaseSelect';

export const StatisticsSelect: React.FC<BaseSelectProps> = ({ className, ...props }) => {
  return (
    <BaseSelect className={className} {...props}>
    </BaseSelect>
  );
};
