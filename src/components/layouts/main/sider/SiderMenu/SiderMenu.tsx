import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import * as S from './SiderMenu.styles';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { MenuItemModel } from '@app/domain/MenuItemModel';
import iconMapper from "@app/utils/mapStringToIcon";

interface SiderContentProps {
  setCollapsed: (isCollapsed: boolean) => void;
}

const SiderMenu: React.FC<SiderContentProps> = ({ setCollapsed }) => {
  const { t } = useTranslation();

  const { menuItems } = useAppSelector(state => state.navigation);

  const sidebarNavFlat = menuItems?.reduce(
    (result: MenuItemModel[], current) =>
      result.concat(current.children && current.children.length > 0 ? current.children : current),
    [],
  );

  const currentMenuItem = sidebarNavFlat?.find(({ url }) => url === location.pathname);
  const defaultSelectedKeys = currentMenuItem ? [currentMenuItem.key] : [];

  const openedSubmenu = menuItems?.find(({ children }) =>
    children?.some(({ url }) => url === location.pathname),
  );
  const defaultOpenKeys = openedSubmenu ? [openedSubmenu.key] : [];

  return (
    
    <S.Menu
      mode="inline"
      defaultSelectedKeys={defaultSelectedKeys}
      defaultOpenKeys={defaultOpenKeys}
      onClick={() => setCollapsed(true)}
      items={menuItems?.map((nav) => {
        const isSubMenu = nav.children?.length;

        return {
          key: nav.key,
          title: t(nav.label),
          label: isSubMenu ? t(nav.label) : <Link to={nav.url || ''}>{t(nav.label)}</Link>,
          icon: iconMapper(nav.icon),
          children:
            isSubMenu &&
            nav.children &&
            nav.children.map((childNav) => ({
              ...childNav,
              label: <Link to={childNav.url || ''}>{t(childNav.label)}</Link>,
              title: t(childNav.label),
              icon: iconMapper(childNav.icon)
            })),
        };
      })}
    />
  );
};

export default SiderMenu;
