import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Group, getAll } from '@app/api/group.api';
import { getAllUser, removeGroupFromUser, updateUser } from '@app/api/user.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseModal } from '@app/components/common/BaseModal/BaseModal';
import { BaseSpace } from '@app/components/common/BaseSpace/BaseSpace';
import { BaseTable } from '@app/components/common/BaseTable/BaseTable';
import { BaseTag } from '@app/components/common/BaseTag/BaseTag';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { notificationController } from '@app/controllers/notificationController';
import { UserType } from '@app/interfaces/UserInterface';
import { Form, Select } from 'antd';
import type { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

export const UserGroupMappingTable: React.FC = () => {
  const { t } = useTranslation();
  const [data, setData] = useState<UserType[]>([]);
  const [group, setGroup] = useState<Group[]>([]);
  const [currentRow, setCurrentRow] = useState<any>();
  const [changeGroup, setChangeGroup] = useState<{
    id: any;
    groupId: any;
    name: string;
    groupName: string;
  }>();

  const [modal2Open, setModal2Open] = useState(false);
  const [updateTrigger, setUpdateTrigger] = useState(false);
  const formRef = React.useRef<FormInstance>(null);
  const [form] = Form.useForm();

  const { Option } = Select;

  const layout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 17 },
  };
  useEffect(() => {
    let isMounted = true;
    fetchUserAndGroupData();
    return () => {
      isMounted = false;
    };
  }, [updateTrigger]);

  const closeEditModal = () => {
    setModal2Open(false);
  };

  const fetchUserAndGroupData = () => {
    getAllUser().then((users) => {
      setData(
        users.filter((user) => {
          return user.groupId !== null;
        }),
      );
    });

    getAll().then((groups) => setGroup(groups));
  };

  const handleUpdate = (record: any, changedGroup: any) => {
    updateUser(record.id, changedGroup)
      .then(() => {
        setUpdateTrigger((prev) => !prev);
        fetchUserAndGroupData();
        notificationController.success({ message: t('common.success') });
      })
      .catch((error) => {
        console.error(error);
      });
    closeEditModal();
  };

  const handleDelete = (record: any) => {
    removeGroupFromUser(record.id, record.groupId)
      .then(() => {
        notificationController.error({ message: t('common.deletedMessage') });
        setUpdateTrigger((prev) => !prev);
        fetchUserAndGroupData();
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleEdit = (record: any) => {
    setCurrentRow(record);
    setModal2Open((state) => !state);
  };

  const columns = [
    {
      title: t('common.serialNumber'),
      dataIndex: 'id',
      width: '10%',
      editable: false,
    },
    {
      title: t('common.user'),
      dataIndex: 'name',
      width: '20%',
      editable: false,
    },
    {
      title: t('forms.userGroupMappingList.goupColumn'),
      dataIndex: 'groupId',
      width: '48%',
      editable: true,
      render: (groupId: any) => {
        const groupData = group.find((group) => group.id === groupId);
        return groupData ? groupData.name : '';
      },
    },
    {
      title: 'Action',
      key: 'action',
      width: '22%',
      render: (_: any, record: any) => (
        <BaseSpace size="middle">
          <BaseButton
            type="primary"
            onClick={() => {
              handleEdit(record);
            }}
          >
            <EditOutlined />
            Edit
          </BaseButton>
          <BaseButton danger onClick={() => handleDelete(record)}>
            <DeleteOutlined />
            Delete
          </BaseButton>

          <BaseModal
            title="Update Group"
            centered
            open={modal2Open}
            onOk={() => handleUpdate(currentRow, changeGroup)}
            onCancel={() => {
              setModal2Open(false);
            }}
          >
            <Form
              {...layout}
              ref={formRef}
              name="userGroupMappingModal"
              labelWrap
              initialValues={{ user: currentRow?.name, group: currentRow?.groupName }}
              form={form}
            >
              <Form.Item name="user" label={t('common.user')}>
                <BaseTag style={{ textAlign: 'center', padding: '5px 10px' }} color="blue">
                  {currentRow?.name}
                </BaseTag>
              </Form.Item>

              <Form.Item
                name="group"
                label={t('forms.userGroupMappingForm.groupList')}
                hasFeedback
                rules={[{ required: true, message: t('forms.userGroupMappingForm.groupError') }]}
              >
                <Select
                  placeholder={t('forms.userGroupMappingForm.selectGroup')}
                  onChange={(e: any) => setChangeGroup(() => e)}
                  value={currentRow?.groupId}
                >
                  {group.map((singlegroup) => (
                    <Option value={singlegroup.id} key={singlegroup.id}>
                      {singlegroup.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Form>
          </BaseModal>
        </BaseSpace>
      ),
    },
  ];

  return (
    <>
      <BaseForm component={false}>
        <BaseTable columns={columns} dataSource={data} bordered />
      </BaseForm>
    </>
  );
};
