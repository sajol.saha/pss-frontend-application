import { Group, getAll } from '@app/api/group.api';
import { getAllUser, updateUser } from '@app/api/user.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseButtonsForm } from '@app/components/common/forms/BaseButtonsForm/BaseButtonsForm';
import { notificationController } from '@app/controllers/notificationController';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { UserType } from './../../../interfaces/UserInterface';
import { BaseSelect, Option } from './../../common/selects/BaseSelect/BaseSelect';

const formItemLayout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

export const UserGroupMappingForm: React.FC = () => {
  const { t } = useTranslation();
  const [isFieldsChanged, setFieldsChanged] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [groups, setGroups] = useState<Group[]>([]);
  const [validUsers, setValidUsers] = useState<UserType[]>([]);
  const [form] = BaseButtonsForm.useForm();

  useEffect(() => {
    getAllUser().then((users) => {
      setValidUsers(
        users.filter((user) => {
          return user.groupId === null;
        }),
      );
    });
  }, []);

  useEffect(() => {
    getAll().then((group) => {
      setGroups(group);
    });
  }, []);

  const onFinish = async (values: any = {}) => {
    setLoading(true);
    try {
      await updateUser(values.user, values.group);
      setValidUsers((prevUsers) => prevUsers.filter((user) => user.id !== values.user));
      notificationController.success({ message: t('common.success') });
      form.resetFields(); 
    } catch (error) {
      notificationController.error({ message: t('common.error') });
    } finally {
      setLoading(false);
    }
  };

  return (
    <BaseCard title={t('forms.userGroupMappingForm.formTitle')} padding={30}>
      <BaseButtonsForm
        {...formItemLayout}
        isFieldsChanged={isFieldsChanged}
        name="userGroupMapping"
        initialValues={{}}
        onFinish={onFinish}
        form={form}
      >
        <BaseRow gutter={[25, 0]}>
          <BaseCol xs={24} xl={12}>
            <BaseButtonsForm.Item
              name="user"
              label={t('forms.userGroupMappingForm.userList')}
              hasFeedback
              rules={[{ required: true, message: t('forms.userGroupMappingForm.userError') }]}
            >
              <BaseSelect placeholder={t('forms.userGroupMappingForm.selectUser')}>
                {validUsers.map((singleUser) => (
                  <Option value={singleUser.id} key={singleUser.id}>
                    {singleUser.name}
                  </Option>
                ))}
              </BaseSelect>
            </BaseButtonsForm.Item>
          </BaseCol>

          <BaseCol xs={24} xl={12}>
            <BaseButtonsForm.Item
              name="group"
              label={t('forms.userGroupMappingForm.groupList')}
              hasFeedback
              rules={[{ required: true, message: t('forms.userGroupMappingForm.groupError') }]}
            >
              <BaseSelect placeholder={t('forms.userGroupMappingForm.selectGroup')}>
                {groups.map((singlegroup) => (
                  <Option value={singlegroup.id} key={singlegroup.id}>
                    {singlegroup.name}
                  </Option>
                ))}
              </BaseSelect>
            </BaseButtonsForm.Item>
          </BaseCol>
        </BaseRow>

        <BaseButtonsForm.Item>
          <BaseButton type="primary" htmlType="submit" loading={isLoading}>
            {t('common.submit')}
          </BaseButton>
        </BaseButtonsForm.Item>
      </BaseButtonsForm>
    </BaseCard>
  );
};
