import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import * as Auth from '@app/components/layouts/AuthLayout/AuthLayout.styles';
import { notificationController } from '@app/controllers/notificationController';
import { useAppDispatch } from '@app/hooks/reduxHooks';
import { doLogin } from '@app/store/slices/authSlice';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate } from 'react-router-dom';
import * as S from './LoginForm.styles';

interface LoginFormData {
  username: string;
  password: string;
}

export const initValues: LoginFormData = {
  username: '',
  password: '',
};

export const LoginForm: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const [isLoading, setLoading] = useState(false);

  const handleSubmit = (values: LoginFormData) => {
    setLoading(true);
    dispatch(doLogin(values))
      .unwrap()
      .then(() => navigate('/'))
      .catch((err) => {
        console.log(err);
        notificationController.error({ message: err.message });
        setLoading(false);
      });
  };

  return (
    <BaseCard padding="1rem">
      <Auth.FormWrapper>
        <BaseForm layout="vertical" onFinish={handleSubmit} requiredMark="optional" initialValues={initValues}>
          <Auth.FormTitle>{t('common.login')}</Auth.FormTitle>
          <Auth.FormItem
            name="username"
            label={'User Name'}
            rules={[{ required: true, message: t('common.requiredField') }]}
          >
            <Auth.FormInput placeholder={'User Name'} />
          </Auth.FormItem>
          <Auth.FormItem
            label={t('common.password')}
            name="password"
            rules={[{ required: true, message: t('common.requiredField') }]}
          >
            <Auth.FormInputPassword placeholder={t('common.password')} />
          </Auth.FormItem>

          <Auth.ActionsWrapper>
            <BaseForm.Item name="rememberMe" valuePropName="checked" noStyle>
              <Auth.FormCheckbox>
                <S.RememberMeText>{t('login.rememberMe')}</S.RememberMeText>
              </Auth.FormCheckbox>
            </BaseForm.Item>
            <Link to="/auth/forgot-password">
              <S.ForgotPasswordText>{t('common.forgotPass')}</S.ForgotPasswordText>
            </Link>
          </Auth.ActionsWrapper>
          <BaseForm.Item noStyle>
            <Auth.SubmitButton type="primary" htmlType="submit" loading={isLoading}>
              {t('common.login')}
            </Auth.SubmitButton>
          </BaseForm.Item>
          <Auth.FooterWrapper>
            <Auth.Text>
              {t('login.noAccount')}{' '}
              <Link to="/auth/sign-up">
                <Auth.LinkText>{t('common.here')}</Auth.LinkText>
              </Link>
            </Auth.Text>
          </Auth.FooterWrapper>
        </BaseForm>
      </Auth.FormWrapper>
    </BaseCard>
  );
};
