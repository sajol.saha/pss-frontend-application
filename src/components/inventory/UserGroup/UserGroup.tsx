

import * as S from '@app/pages/uiComponentsPages//UIComponentsPage.styles';
import { useState } from 'react';
import { Form, Input, Select, Button, Table, Space, Popconfirm } from 'antd';
import { FormInstance } from 'antd/lib/form';

const { Option } = Select;

type Props = {}


interface RowData {
  key: string;
  inputField: string;
  selectValue: string;
}



const UserGroup = (props: Props) => {

  

  const [form] = Form.useForm<FormInstance>();
  const [tableData, setTableData] = useState<RowData[]>([]);
  const [editingRow, setEditingRow] = useState<RowData | null>(null);



  const onFinish = (values: RowData) => {
    if (editingRow) {
      const updatedData = tableData.map((row) =>
        row.key === editingRow.key ? { ...row, ...values } : row
      );
      setTableData(updatedData);
      setEditingRow(null);
    } else {
      const newData: RowData = {
        ...values,
        key: Date.now().toString()
      };
      setTableData([...tableData, newData]);
    }
    form.resetFields();
  };


  const handleEdit = (row: RowData) => {
    form.setFieldsValue(row);
    setEditingRow(row);
  };

  const handleDelete = (key: string) => {
    const filteredData = tableData.filter((row) => row.key !== key);
    setTableData(filteredData);
  };
  const columns = [
    {
      title: 'Input Field',
      dataIndex: 'inputField',
      key: 'inputField',
    },
    {
      title: 'Select Value',
      dataIndex: 'selectValue',
      key: 'selectValue',
    },
    {
      title: 'Actions',
      key: 'actions',
      render: (_, row: RowData) => (
        <Space>
          <Button type="link" onClick={() => handleEdit(row)}>
            Edit
          </Button>

          <Popconfirm
            title="Are you sure you want to delete this row?"
            onConfirm={() => handleDelete(row.key)}
          >

            <Button type="link">Delete</Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <S.Card title={'USER FORM'}>
        <div>

        <Form form={form} onFinish={onFinish}>
        <Form.Item name="inputField" label="Input Field">
          <Input />
        </Form.Item>
        <Form.Item name="selectValue" label="Select Value">
          <Select>
            <Option value="option1">Option 1</Option>
            <Option value="option2">Option 2</Option>
            <Option value="option3">Option 3</Option>
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            {editingRow ? 'Update' : 'Add to Table'}
          </Button>
        </Form.Item>
      </Form>
          <Table dataSource={tableData} columns={columns} />
        </div>
      </S.Card>
    </>
  );
};

export default UserGroup;

