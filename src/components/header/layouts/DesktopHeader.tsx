import React, { useState } from 'react';
import { NotificationsDropdown } from '../components/notificationsDropdown/NotificationsDropdown';
import { ProfileDropdown } from '../components/profileDropdown/ProfileDropdown/ProfileDropdown';
import { HeaderSearch } from '../components/HeaderSearch/HeaderSearch';
import { SettingsDropdown } from '../components/settingsDropdown/SettingsDropdown';
import { HeaderFullscreen } from '../components/HeaderFullscreen/HeaderFullscreen';
import * as S from '../Header.styles';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { Menu } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '@app/hooks/reduxHooks';
import { MenuItemModel } from '@app/domain/MenuItemModel';
import iconMapper from "@app/utils/mapStringToIcon";

interface DesktopHeaderProps {
  isTwoColumnsLayout: boolean;
}

export const DesktopHeader: React.FC<DesktopHeaderProps> = ({ isTwoColumnsLayout }) => {

  const { menuItems } = useAppSelector(state => state.navigation);

  const navigate = useNavigate();
  const [current, setCurrent] = useState('mail');

  const theme = useAppSelector((state) => state.theme.theme);
  const onClick = (e: { key: React.SetStateAction<string> }) => {

    setCurrent(e.key);
    const navigationUrl = findUrlByKey(items, e.key + '');
    navigate('' + navigationUrl);
    
  };

  function findUrlByKey(items: MenuItemModel[], key: string): string | undefined {
    for (const item of items) {
      if (item.key === key && item.url) {
        return item.url;
      }

      if (item.children) {
        const url = findUrlByKey(item.children, key);
        if (url) {
          return url;
        }
      }
    }
  }

  const items:MenuItemModel[] = menuItems ? menuItems : [] as MenuItemModel[];

  const leftSide = isTwoColumnsLayout ? (
    <S.SearchColumn xl={16} xxl={17}>
      <BaseRow justify="space-between">
        <BaseCol xl={15} xxl={12}>
          <HeaderSearch />
        </BaseCol>
        <BaseCol>
          <S.GHButton />
        </BaseCol>
      </BaseRow>
    </S.SearchColumn>
  ) : (
    <>
      <BaseCol lg={10} xxl={8}>
        <Menu
          style={{ borderColor: 'transparent', width: '1050px' }}
          theme={theme === 'dark' ? 'dark' : 'light'}
          onClick={onClick}
          selectedKeys={[current]}
          mode="horizontal"
          items={ items.map(item => {
            return {
              ...item,
              icon: iconMapper(item.icon),
              children: item.children?.map(child => {
                return { ...child, icon: iconMapper(child.icon) }
              })
            }
          }) 
          }
        />
      </BaseCol>
    </>
  );

  return (
    <BaseRow justify="space-between" align="middle">
      {leftSide}

      <S.ProfileColumn xl={8} xxl={7} $isTwoColumnsLayout={isTwoColumnsLayout}>
        <BaseRow align="middle" justify="end" gutter={[5, 5]}>
          <BaseCol>
            <BaseRow gutter={[{ xxl: 5 }, { xxl: 5 }]}>
              <BaseCol>
                <HeaderFullscreen />
              </BaseCol>

              <BaseCol>
                <NotificationsDropdown />
              </BaseCol>

              <BaseCol>
                <SettingsDropdown />
              </BaseCol>
            </BaseRow>
          </BaseCol>

          <BaseCol>
            <ProfileDropdown />
          </BaseCol>
        </BaseRow>
      </S.ProfileColumn>
    </BaseRow>
  );
};
