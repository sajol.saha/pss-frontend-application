import { ApiError } from '@app/api/ApiError';
import { readToken } from '@app/services/localStorage.service';
import axios, { AxiosError } from 'axios';

export const httpApi = axios.create({
  baseURL: 'http://localhost:8080/api/v1',
});

// Gateway endpoint
export const customApiBaseUrl = 'http://localhost:8080/api/v1';

httpApi.interceptors.request.use((config) => {
  config.headers = { ...config.headers, Authorization: `Bearer ${readToken()}` };
  console.log(JSON.stringify(config));
  return config;
});

httpApi.interceptors.response.use(undefined, (error: AxiosError) => {
  throw new ApiError<ApiErrorData>(error.response?.data.message || error.message, error.response?.data);
});

export interface ApiErrorData {
  message: string;
}
