import { customApiBaseUrl } from '@app/api/http.api';
import { readToken } from '@app/services/localStorage.service';
import axios from 'axios';

export type RoleResponse = {
    id: number,
    name: string,
    description: string
}

export type Group = {
    id: number,
    name: string
    roleResponses: RoleResponse[]
}

export type RoleSaveRequest = {
    groupRoleIds: number[]
}

export type RoleUpdateRequest = RoleSaveRequest;

const groupUrl = `${customApiBaseUrl}/groups`

export const getAll = async (): Promise<Group[]> => {
    const { data } = await axios({
        method: "GET",
        url: groupUrl,
        headers: { Authorization: `Bearer ${readToken()}` },
        timeout: 10000
    });
    return data;
}

export const saveRoles = async (groupId: number, roleIdList:number[]): Promise<void> => {

    const payload: RoleSaveRequest = {
        groupRoleIds: roleIdList
    }

    await axios({
        method: "POST",
        url: `${groupUrl}/${groupId}/roles`,
        data: payload,
        headers: { Authorization: `Bearer ${readToken()}` },
        timeout: 10000
    });
}

export const updateRoles = async (groupId: number, roleIdList:number[]): Promise<void> => {

    const payload: RoleUpdateRequest = {
        groupRoleIds: roleIdList
    }

    await axios({
        method: "PATCH",
        url: `${groupUrl}/${groupId}/roles`,
        data: payload,
        headers: { Authorization: `Bearer ${readToken()}` },
        timeout: 10000
    });
}