import axios, { AxiosPromise } from 'axios';
import { customApiBaseUrl, httpApi } from '@app/api/http.api';
import { UserModel } from '@app/domain/UserModel';
import { readRefreshToken, readToken } from '@app/services/localStorage.service';


export interface SignUpRequest {
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
}

export interface ResetPasswordRequest {
  email: string;
}

export interface SecurityCodePayload {
  code: string;
}

export interface NewPasswordData {
  newPassword: string;
}

export interface LoginRequest {
  username: string;
  password: string;
}

export type MenuItemsResponse = {
  id: number,
  name: string,
  url: string,
  icon: string,
  serviceId: number,
  serviceName: string
}

export interface LoginResponse {
  accessToken: string;
  refreshToken: string;
  user: UserModel;
  status: number;
  remarks: string;
  menuResponseSet: MenuItemsResponse[];
}

const loginEndpoint = `${customApiBaseUrl}` + '/auth/login';
const logoutEndpoint = `${customApiBaseUrl}` + '/auth/logout';
const registerEndpoint = `${customApiBaseUrl}` + '/users';

export const login = (loginPayload: LoginRequest): Promise<LoginResponse> =>
  axios({
    method: 'POST',
    url: loginEndpoint,
    data: loginPayload,
    timeout: 10000,
  }).then(({ data }) => data);

export const logout = (): AxiosPromise =>
  axios({
    method: 'POST',
    url: logoutEndpoint,
    headers: {
      apiKey: `auth-one`,
      Authorization: `Bearer ${readToken()}`,
    },
    data: {
      refreshToken: readRefreshToken(),
    },
    timeout: 10000,
  });

export const signUp = (signUpData: SignUpRequest): Promise<undefined> =>
  axios({
    method: 'POST',
    url: registerEndpoint,
    headers: { apiKey: `auth-one` },
    data: signUpData,
    timeout: 10000,
  }).then(({ data }) => data);

export const resetPassword = (resetPasswordPayload: ResetPasswordRequest): Promise<undefined> =>
  httpApi.post<undefined>('forgotPassword', { ...resetPasswordPayload }).then(({ data }) => data);

export const verifySecurityCode = (securityCodePayload: SecurityCodePayload): Promise<undefined> =>
  httpApi.post<undefined>('verifySecurityCode', { ...securityCodePayload }).then(({ data }) => data);

export const setNewPassword = (newPasswordData: NewPasswordData): Promise<undefined> =>
  httpApi.post<undefined>('setNewPassword', { ...newPasswordData }).then(({ data }) => data);
