import { customApiBaseUrl } from '@app/api/http.api';
import { readToken } from '@app/services/localStorage.service';
import axios from 'axios';

export type Role = {
    id: number,
    name: string,
    description: string
}

const roleUrl = `${customApiBaseUrl}/roles`

export const getAll = async (): Promise<Role[]> => {
    const { data } = await axios({
        method: "GET",
        url: roleUrl,
        headers: { Authorization: `Bearer ${readToken()}` },
        timeout: 10000
    });
    return data;
}