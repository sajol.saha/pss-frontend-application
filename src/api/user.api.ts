import { UserType } from '@app/interfaces/UserInterface';
import { readToken } from '@app/services/localStorage.service';
import axios from 'axios';
import { customApiBaseUrl } from './http.api';

type UserUpdate = {
  userId: number;
  groupId: number | undefined;
};

const userURL = `${customApiBaseUrl}/users`;
const userGroupURL = `${userURL}/group/assign`;
const userGroupRemoveURL = `${userURL}/group/remove`;

export const getAllUser = async (): Promise<UserType[]> => {
  const { data } = await axios({
    method: 'GET',
    url: userURL,
    headers: { Authorization: `Bearer ${readToken()}` },
  });
  return data;
};

export const updateUser = async (uId: number, gId: number) => {
  const payload: UserUpdate = {
    userId: uId,
    groupId: gId,
  };

  await axios({
    method: 'PUT',
    url: userGroupURL,
    data: payload,
    headers: { Authorization: `Bearer ${readToken()}` },
  });
};

export const removeGroupFromUser = async (uId: number, gId: number | undefined) => {
  const payload: UserUpdate = {
    userId: uId,
    groupId: gId,
  };

  await axios({
    method: 'PUT',
    url: userGroupRemoveURL,
    data: payload,
    headers: { Authorization: `Bearer ${readToken()}` },
  });
};
