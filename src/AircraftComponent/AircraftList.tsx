import { EditOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { customApiBaseUrl as baseURL } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { notificationController } from '@app/controllers/notificationController';
import { readToken } from '@app/services/localStorage.service';
import { Table as AntTable, Space } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import '../Root.css';

const AircraftList: React.FC = () => {
  const [aircrafts, setAircrafts] = useState();

  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('accessToken');

    const payload = {};

    axios({
      method: 'POST',
      url: `${baseURL}/aircraft/get`,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: payload,
      timeout: 60000,
    })
      .then((response) => {
        setAircrafts(response.data);
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.data != null && err.response.data.length > 0) {
            notificationController.error({ message: err.response.data });
          } else if (err.response.data.message != null) {
            notificationController.error({ message: err.response.data.message });
          } else if (err.message != null) {
            notificationController.error({ message: err.message });
          }
        }
      });
  }, []);

  const navigateToAircraft = () => {
    navigate('/aircraft');
  };

  const goToEdit = (id: string | number) => {
    fetchAircraft(id);
  };

  function fetchAircraft(aircraftId: number | string) {
    const payload = { id: aircraftId };
    const token = readToken();

    axios({
      method: 'POST',
      url: `${baseURL}/aircraft/get`,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: payload,
      timeout: 60000,
    }).then((response) => {
      if (response.data != null && response.data.length > 0) {
        navigate('/aircraft', { state: { aircraftId: aircraftId, aircraftModel: response.data[0] } });
      }
    });
  }

  const { t } = useTranslation();

  interface DataType {
    id: number;
    name: string;
    totalSeats: number;
  }

  const columns: ColumnsType<DataType> = [
    {
      title: 'Aircraft Name',
      dataIndex: 'name',
      key: 'id',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Total Seats',
      dataIndex: 'totalSeats',
      key: 'totalSeats',
    },
    {
      title: 'Action',
      key: 'action',
      render: () => (
        <Space size="middle">
          <EditOutlined />
          <a>Edit</a>
        </Space>
      ),
    },
  ];

  return (
    <>
      <PageTitle>{t('pss.aircraftList')}</PageTitle>

      <BaseButton type="primary" onClick={navigateToAircraft} style={{ marginBottom: 25, verticalAlign: 'middle' }}>
        <PlusCircleOutlined />
        Add New Aircraft
      </BaseButton>

      <AntTable
        columns={columns}
        dataSource={aircrafts}
        onRow={(record) => {
          return {
            onClick: () => {
              goToEdit(record.id);
            },
          };
        }}
      />
    </>
  );
};

export default AircraftList;
