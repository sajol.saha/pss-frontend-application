import { useLocation, useNavigate } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { readToken } from '@app/services/localStorage.service';
import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { BaseInput } from '@app/components/common/inputs/BaseInput/BaseInput';
import { BaseButtonsForm } from '@app/components/common/forms/BaseButtonsForm/BaseButtonsForm';
import { useTranslation } from 'react-i18next';
import { notificationController } from '@app/controllers/notificationController';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { InputNumber } from '@app/components/common/inputs/InputNumber/InputNumber';
import { customApiBaseUrl as baseURL } from '@app/api/http.api';

const AircraftType: React.FC = () => {
  const { t } = useTranslation();

  const location = useLocation();

  const navigate = useNavigate();

  interface aircraftTypeModel {
    id?: number;
    name: string;
    avgManWeight: number;
    avgWomanWeight: number;
    avgChildWeight: number;
    avgInfantWeight: number;
  }

  const initAircraftValues = {
    id: 0,
    name: '',
    avgManWeight: 0,
    avgWomanWeight: 0,
    avgChildWeight: 0,
    avgInfantWeight: 0,
  };

  const [aircraftType, setAircraftType] = useState(
    location.state?.aircraftModel != null ? location.state.aircraftModel : initAircraftValues,
  );

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (
      location != null &&
      location.state != null &&
      location.state.aircraftTypeId != null &&
      location.state.aircraftTypeId > 0
    ) {
      setAircraftType(location.state.aircraftTypeModel);
    }
  }, [location]);

  const onSubmit = () => {
    const endpoint = `${baseURL}` + '/aircraft/type/add';

    const payload = aircraftTypeData;

    if(payload.id) {
      if (!(payload?.id > 0)) {
        delete payload.id;
      }
    }

    setLoading(true);

    axios({
      method: 'POST',
      url: endpoint,
      data: payload,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${readToken()}`,
      },
      timeout: 60000,
    })
      .then(() => {
        setLoading(false);
        notificationController.success({ message: t('common.success') });
        navigate('/aircraft-type-list');
      })
      .catch((err) => {
        setLoading(false);
        notificationController.error({ message: err.message });
      });
  };

  const [aircraftTypeData, setAircraftTypeData] = useState<aircraftTypeModel>(aircraftType);

  return (
    <>
      <PageTitle>Aircraft Type</PageTitle>
      <BaseCard>
        <BaseForm
          layout="vertical"
          initialValues={aircraftType}
          onValuesChange={(field) => {
            const values = Object.entries(field)[0];
            setAircraftTypeData({ ...aircraftTypeData, [values[0]]: values[1] });
          }}
        >
          <BaseButtonsForm.Item name="name" label={'Aircraft Type'}>
            <BaseInput style={{ width: 500 }} />
          </BaseButtonsForm.Item>
          <BaseButtonsForm.Item name="avgManWeight" label={'Average Man Weight'}>
            <InputNumber style={{ width: 120 }} />
          </BaseButtonsForm.Item>
          <BaseButtonsForm.Item name="avgWomanWeight" label={'Average Woman Weight'}>
            <InputNumber style={{ width: 120 }} />
          </BaseButtonsForm.Item>
          <BaseButtonsForm.Item name="avgChildWeight" label={'Average Child Weight'}>
            <InputNumber style={{ width: 120 }} />
          </BaseButtonsForm.Item>
          <BaseButtonsForm.Item name="avgInfantWeight" label={'Average Infant Weight'}>
            <InputNumber style={{ width: 120 }} />
          </BaseButtonsForm.Item>
          <BaseButton type="primary" loading={isLoading} disabled={isLoading} onClick={() => onSubmit()}>
            Save
          </BaseButton>
        </BaseForm>
      </BaseCard>
    </>
  );
};

export default AircraftType;
