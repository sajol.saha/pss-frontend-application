import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { useTranslation } from 'react-i18next';
import { customApiBaseUrl as baseURL } from '@app/api/http.api';
import { readToken } from '@app/services/localStorage.service';
import { PlusCircleOutlined, EditOutlined } from '@ant-design/icons';
import { Space, Table as AntTable } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { notificationController } from '@app/controllers/notificationController';

const AircraftTypeList: React.FC = () => {
  const [aircraftTypes, setAircraftTypes] = useState([]);

  const navigate = useNavigate();

  useEffect(() => {
    const endpoint = `${baseURL}` + '/aircraft/type/get';
    const token = localStorage.getItem('accessToken');
    const payload = {};
    axios({
      method: 'POST',
      url: endpoint,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: payload,
      timeout: 60000,
    })
      .then((response) => {
        setAircraftTypes(response.data);
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.data != null && err.response.data.length > 0) {
            notificationController.error({ message: err.response.data });
          } else if (err.response.data.message != null) {
            notificationController.error({ message: err.response.data.message });
          } else if (err.message != null) {
            notificationController.error({ message: err.message });
          }
        }
      });
  }, []);

  const navigateToAircraftType = () => {
    navigate('/aircraft-type');
  };

  const goToEdit = (id: string | number) => {
    fetchAircraftType(id);
  };

  function fetchAircraftType(aircraftTypeId: number | string) {
    const payload = { id: aircraftTypeId };
    const token = readToken();

    axios({
      method: 'POST',
      url: `${baseURL}/aircraft/type/get`,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: payload,
      timeout: 60000,
    })
      .then((response) => {
        if (response.data != null && response.data.length > 0) {
          navigate('/aircraft-type', { state: { aircraftId: aircraftTypeId, aircraftModel: response.data[0] } });
        }
      });
  }

  const { t } = useTranslation();

  interface DataType {
    id: number;
    name: string;
    totalSeats: number;
  }

  const columns: ColumnsType<DataType> = [
    {
      title: 'Aircraft Type',
      dataIndex: 'name',
      key: 'id',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Avg Man Weight',
      dataIndex: 'avgManWeight',
      key: 'avgManWeight',
    },
    {
      title: 'Avg Woman Weight',
      dataIndex: 'avgWomanWeight',
      key: 'avgWomanWeight',
    },
    {
      title: 'Avg Child Weight',
      dataIndex: 'avgChildWeight',
      key: 'avgChildWeight',
    },

    {
      title: 'Avg Infant Weight',
      dataIndex: 'avgInfantWeight',
      key: 'avgInfantWeight',
    },
    {
      title: 'Action',
      key: 'action',
      render: () => (
        <Space size="middle">
          <EditOutlined />
          <a>Edit</a>
        </Space>
      ),
    },
  ];

  return (
    <>
      <PageTitle>{t('pss.aircraftList')}</PageTitle>

      <BaseButton type="primary" onClick={navigateToAircraftType} style={{ marginBottom: 25, verticalAlign: 'middle' }}>
        <PlusCircleOutlined />
        Add New Aircraft Type
      </BaseButton>

      <AntTable
        columns={columns}
        dataSource={aircraftTypes}
        onRow={(record) => {
          return {
            onClick: () => {
              goToEdit(record.id);
            },
          };
        }}
      />
    </>
  );
};

export default AircraftTypeList;
