import { customApiBaseUrl as baseURL } from '@app/api/http.api';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { BaseCard } from '@app/components/common/BaseCard/BaseCard';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
import { BaseButtonsForm } from '@app/components/common/forms/BaseButtonsForm/BaseButtonsForm';
import { BaseForm } from '@app/components/common/forms/BaseForm/BaseForm';
import { BaseInput } from '@app/components/common/inputs/BaseInput/BaseInput';
import { InputNumber } from '@app/components/common/inputs/InputNumber/InputNumber';
import { notificationController } from '@app/controllers/notificationController';
import { readToken } from '@app/services/localStorage.service';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';

const Aircraft: React.FC = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();

  interface aircraftModel {
    id: number;
    name: string;
    totalSeats: number;
  }

  const initAircraftValues = {
    id: 0,
    name: '',
    totalSeats: 0,
  };

  const [aircraft, setAircraft] = useState(
    location.state?.aircraftModel != null ? location.state.aircraftModel : initAircraftValues,
  );

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (
      location != null &&
      location.state != null &&
      location.state.aircraftId != null &&
      location.state.aircraftId > 0
    ) {
      setAircraft(location.state.aircraftModel);
    }
  }, [location]);

  const onSubmit = () => {
    const endpoint = `${baseURL}` + '/aircraft/add';

    const payload = aircraftData;

    if (!(payload?.id > 0)) {
    }

    setLoading(true);

    axios({
      method: 'POST',
      url: endpoint,
      data: payload,
      headers: {
        apiKey: `auth-one`,
        Accept: 'application/json',
        Authorization: `Bearer ${readToken()}`,
      },
      timeout: 60000,
    })
      .then(() => {
        setLoading(false);
        notificationController.success({ message: t('common.success') });
        navigate('/aircraft-list');
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.data != null && err.response.data.length > 0) {
            notificationController.error({ message: err.response.data });
          } else if (err.response.data.message != null) {
            notificationController.error({ message: err.response.data.message });
          } else if (err.message != null) {
            notificationController.error({ message: err.message });
          }
        }
        setLoading(false);
      });
  };

  const [aircraftData, setAircraftData] = useState<aircraftModel>(aircraft);

  return (
    <>
      <PageTitle>{t('pss.aircraft')}</PageTitle>
      <BaseCard>
        <BaseForm
          layout="vertical"
          initialValues={aircraft}
          onValuesChange={(field) => {
            const values = Object.entries(field)[0];

            setAircraftData({ ...aircraftData, [values[0]]: values[1] });
          }}
        >
          <BaseButtonsForm.Item name="name" label={'Aircraft Name'}>
            <BaseInput style={{ width: 500 }} />
          </BaseButtonsForm.Item>
          <BaseButtonsForm.Item name="totalSeats" label={'Total Seats'}>
            <InputNumber style={{ width: 120 }} />
          </BaseButtonsForm.Item>
          <BaseButton type="primary" loading={isLoading} onClick={() => onSubmit()}>
            Save
          </BaseButton>
        </BaseForm>
      </BaseCard>
    </>
  );
};

export default Aircraft;
