import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { readToken } from '../localStorage.service';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({ 
    baseUrl: 'http://localhost:8080/api/v1' ,
    prepareHeaders: (headers) => {
      headers.set("Content-type", "application/json"),
      headers.set("Authorization", "Bearer " + readToken());
      return headers;
    },
  }
  ),
  tagTypes: ['api'],
  endpoints: (builder) => ({}),
});
