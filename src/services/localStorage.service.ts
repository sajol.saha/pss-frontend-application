import { MenuItemModel } from '@app/domain/MenuItemModel';
import { UserModel } from '@app/domain/UserModel';
const avatarImg = process.env.REACT_APP_ASSETS_BUCKET + '/avatars/avatar5.webp';

const testUser = {
  id: 1,
  firstName: 'Chris',
  lastName: 'Johnson',
  imgUrl: avatarImg,
  userName: '@john1989',
  email: {
    name: 'chris.johnson@altence.com',
    verified: true,
  },
  phone: {
    number: '+18143519459',
    verified: false,
  },
  sex: 'male',
  birthday: '01/26/2022',
  lang: 'en',
  country: 'GB',
  city: 'London',
  address1: '14 London Road',
  zipcode: 5211,
  website: 'altence.com',
  socials: {
    twitter: '@altence_team',
    facebook: 'https://facebook.com/groups/1076577369582221',
    linkedin: 'https://linkedin.com/company/altence',
  },
};

export const persistToken = (token: string): void => {
  localStorage.setItem('accessToken', token);
};

export const persistRefreshToken = (refreshToken: string): void => {
  localStorage.setItem('refreshToken', refreshToken);
};

export const readToken = (): string => {
  return localStorage.getItem('accessToken') || 'bearerToken';
};

export const readRefreshToken = (): string => {
  return localStorage.getItem('refreshToken') || 'refreshToken';
};

export const persistUser = (user: UserModel): void => {
  localStorage.setItem('user', JSON.stringify(user));
};

export const readUser = (): UserModel | null => {
  const userStr = localStorage.getItem('user');

  return userStr ? JSON.parse(userStr) : testUser;
};

export const persistMenuItems = (navItems: MenuItemModel[]): void => {
  localStorage.setItem('navItems', JSON.stringify(navItems));
}

export const readMenuItems = (): MenuItemModel[] | null => {
  const navItemsStr = localStorage.getItem('navItems');
  return navItemsStr ? JSON.parse(navItemsStr) : null;
};

export const deleteToken = (): void => localStorage.removeItem('accessToken');
export const deleteRefreshToken = (): void => localStorage.removeItem('refreshToken');
export const deleteUser = (): void => localStorage.removeItem('user');
export const deleteMenuItems = (): void => localStorage.removeItem('navItems');
