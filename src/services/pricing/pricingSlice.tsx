import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  test2: [],
};

const pricingSlice = createSlice({
  name: 'second',
  initialState,
  reducers: {},
});

export const {} = pricingSlice.actions;

export default pricingSlice.reducer;
