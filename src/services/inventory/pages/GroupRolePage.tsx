import { useTranslation } from "react-i18next";
import { PageTitle } from "@app/components/common/PageTitle/PageTitle";
import { BaseRow } from "@app/components/common/BaseRow/BaseRow";
import { BaseTable } from "@app/components/common/BaseTable/BaseTable";
import { useCallback, useEffect, useState } from "react";
import { BaseCol } from "@app/components/common/BaseCol/BaseCol";
import { BaseButton } from "@app/components/common/BaseButton/BaseButton";
import { BaseSpace } from "@app/components/common/BaseSpace/BaseSpace";
import { BaseModal } from "@app/components/common/BaseModal/BaseModal";
import { Select } from "antd";
import * as S from '@app/pages/uiComponentsPages//UIComponentsPage.styles';
import { BaseSelect } from "@app/components/common/selects/BaseSelect/BaseSelect";
import { 
    Group, 
    getAll as getAllGroups, 
    saveRoles as saveRolesToGroup,
    updateRoles as updateRolesInGroup
} from "@app/api/group.api";
import { Status } from '@app/components/profile/profileCard/profileFormNav/nav/payments/paymentHistory/Status/Status';
import { Role, getAll as getAllRoles } from "@app/api/role.api";

const GroupRolePage:React.FC = () => {

    const { t } = useTranslation();
    const { Option } = Select;

    
    type MappingData = {
        group: number,
        roles: number[]
    }
    const [tempMapping, setTempMapping] = useState<MappingData>({} as MappingData);
    const [isAddModalOpen, setIsAddModalOpen] = useState<boolean>(false);
    const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false);
    

    const [groups, setGroups] = useState<Group[]>([] as Group[]);
    const [roles, setRoles] = useState<Role[]>();

    const getGroups = useCallback((): void => {
        getAllGroups().then(data => {
            const statusFormat = data.map(d => ({...d, value: d.name.toLowerCase(), label: d.name}));
            setGroups(statusFormat);
        });
    }, []);

    const getRoles = useCallback((): void => {
        getAllRoles().then(data => {
            const statusFormat = data.map(d => ({...d, value: d.name.toLowerCase(), label: d.name}));
            setRoles(statusFormat);
        })
    }, []);

    useEffect(() => {
        getGroups();
        getRoles();
    }, [getGroups, getRoles]);

    type Column = {
        key: string,
        title: string,
        dataIndex?: string,
        render?: (param: any) => JSX.Element
    }
    const columns: Column[] = [
        {
            key: "1",
            title: "Group",
            dataIndex: "name"
        },
        {
            key: "2",
            title: "Role",
            dataIndex: "roleResponses",
            render: (roles: Role[]) => (
                <BaseRow gutter={[10, 10]}>
                    {
                        roles?.map(role => (
                            <BaseCol key={role.id}>
                                <Status color="" text={role?.name?.toUpperCase()} />
                            </BaseCol>
                        ))
                    }
                </BaseRow>
            )
        },
        {
            key: "3",
            title: "Action",
            render: (record: Group) => {
                return (
                    <BaseSpace>
                    {
                        record
                        ?
                        <BaseButton type="primary" onClick={() => { handleEdit(record) }}>Edit</BaseButton>
                        :
                        null
                    }
                    </BaseSpace>
                )
            }
        }
    ];

    const resetTempMapping = () => {
        setTempMapping({} as MappingData);
    }

    const handleAdd = () => {
        setIsAddModalOpen(true);
        resetTempMapping();
    }

    const confirmSave = (mapping: MappingData) => {   
        const roleIdList = mapping.roles;
        const groupId = mapping.group;
        if(groupId && roleIdList.length > 0) {
            saveRolesToGroup(groupId, roleIdList).then(() => getGroups());
            setIsAddModalOpen(false);
        }
    }

    const handleEdit = (record: Group) => {
        setIsEditModalOpen(true);
        setTempMapping({ group: record.id, roles: record.roleResponses.map(role => role.id) });
    }

    const confirmEdit = (mapping: MappingData) => {
        const roleIdList = mapping.roles;
        const groupId = mapping.group;
        if(groupId && roleIdList.length > 0) {
            updateRolesInGroup(groupId, roleIdList).then(() => getGroups());
            setIsEditModalOpen(false);
        }
    }

    return (
        <>
            <PageTitle>{t('admin.group-role')}</PageTitle>
            <BaseRow>
                <BaseCol xs={24}>

                    <BaseModal
                        centered
                        title={t("Add new mapping")}
                        open={isAddModalOpen}
                        onOk={() => { confirmSave(tempMapping) }}
                        onCancel={() => { setIsAddModalOpen(false); resetTempMapping() }}
                    >
                        <BaseSpace>
                            <S.Card title={t('Select Group')}>
                                <BaseSelect
                                    value={tempMapping.group}
                                    onChange={(e) => setTempMapping(prev => ({...prev, group: e} as MappingData))}
                                    width={120}
                                >
                                    {
                                        groups?.map(group => (
                                            <Option 
                                                disabled={group.roleResponses.length > 0} 
                                                key={group.id} 
                                                value={group.id}
                                            >
                                                {group.name}
                                            </Option>
                                        ))
                                    }
                                </BaseSelect>
                            </S.Card>
                            <S.Card title={t('Select Role')}>
                                <BaseSelect
                                    mode="multiple"
                                    value={tempMapping.roles}
                                    onChange={(e) => setTempMapping(prev => ({...prev, roles: e} as MappingData))}
                                    width={120}
                                >
                                    {
                                        roles?.map(role => (
                                            <Option key={role.id} value={role.id}>
                                                {role.name}
                                            </Option>
                                        ))
                                    }    
                                </BaseSelect>
                            </S.Card>
                        </BaseSpace>
                    </BaseModal>

                    <BaseButton type="primary" onClick={() => { handleAdd() }}>Add new mapping</BaseButton>
                    <BaseTable
                        rowKey={"id"}
                        columns={columns}
                        dataSource={groups}
                        bordered
                    />

                    <BaseModal
                        centered
                        title={t("Edit mapping")}
                        open={isEditModalOpen}
                        onOk={() => { confirmEdit(tempMapping) }}
                        onCancel={() => { setIsEditModalOpen(false); resetTempMapping() }}
                    >
                        <BaseSpace>
                            <S.Card title={t('Select Group')}>
                                <BaseSelect
                                    disabled
                                    value={tempMapping.group}
                                    onChange={(e) => setTempMapping(prev => ({...prev, group: e} as MappingData))}
                                    width={120}
                                >
                                    {
                                        groups?.map(group => (
                                            <Option
                                                key={group.id} 
                                                value={group.id}
                                            >
                                                {group.name}
                                            </Option>
                                        ))
                                    }
                                </BaseSelect>
                            </S.Card>
                            <S.Card title={t('Select Role')}>
                                <BaseSelect
                                    mode="multiple"
                                    value={tempMapping.roles}
                                    onChange={(e) => setTempMapping(prev => ({...prev, roles: e} as MappingData))}
                                    width={120}
                                >
                                    {
                                        roles?.map(role => (
                                            <Option key={role.id} value={role.id}>
                                                {role.name}
                                            </Option>
                                        ))
                                    }    
                                </BaseSelect>
                            </S.Card>
                        </BaseSpace>
                    </BaseModal>

                </BaseCol>
            </BaseRow>
        </>
    )
}

export default GroupRolePage;