import {
    DashboardOutlined,
    LineChartOutlined,
    TableOutlined,
    UserOutlined,
    DingdingOutlined,
    DollarOutlined,
    MessageOutlined,
    CreditCardOutlined,
    ProfileOutlined,
  } from '@ant-design/icons';
  import { ReactComponent as NftIcon } from '@app/assets/icons/nft-icon.svg';
import { ReactElement } from 'react';

const mapStringToIcon = (iconName: string | null = null): ReactElement => {
    
    const icons = {
        NftIcon: <NftIcon />,
        DashboardOutlined: <DashboardOutlined />,
        TableOutlined: <TableOutlined />,
        LineChartOutlined: <LineChartOutlined />,
        DingdingOutlined: <DingdingOutlined />,
        DollarOutlined: <DollarOutlined />,
        CreditCardOutlined: <CreditCardOutlined />,
        MessageOutlined: <MessageOutlined />,
        ProfileOutlined: <ProfileOutlined />,
        UserOutlined: <UserOutlined />,
    }
    return icons[iconName as keyof typeof icons];
}

export default mapStringToIcon;