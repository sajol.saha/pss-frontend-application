import { notificationController } from '@app/controllers/notificationController';
import { isRejectedWithValue, Middleware } from '@reduxjs/toolkit';

export const errorLoggingMiddleware: Middleware = () => (next) => (action) => {
  if (isRejectedWithValue(action)) {
    const errorMessage = action.payload?.data?.message;
    if (errorMessage) {
      notificationController.error({ message: errorMessage });
    }
  }

  return next(action);
};
