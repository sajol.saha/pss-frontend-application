import userReducer from '@app/store/slices/userSlice';
import authReducer from '@app/store/slices/authSlice';
import nightModeReducer from '@app/store/slices/nightModeSlice';
import themeReducer from '@app/store/slices/themeSlice';
import pwaReducer from '@app/store/slices/pwaSlice';
import navigationReducer from '@app/store/slices/navigationSlice';
import { apiSlice } from '@app/services/api/apiSlice';
import  roleReducer  from '@app/systemManagement/roleManagement/roleSlice';


export default {
  [apiSlice.reducerPath]: apiSlice.reducer,
  role: roleReducer,
  user: userReducer,
  auth: authReducer,
  nightMode: nightModeReducer,
  theme: themeReducer,
  pwa: pwaReducer,
  navigation: navigationReducer
};