
import { MenuItemsResponse } from "@app/api/auth.api";
import { MenuItemModel } from "@app/domain/MenuItemModel";
import { persistMenuItems, readMenuItems } from "@app/services/localStorage.service";
import { createSlice } from "@reduxjs/toolkit";


export interface NavigationSlice {
    items: MenuItemModel[] | null;
}

const initialState = {
    menuItems: readMenuItems(),
}

export const navigationSlice = createSlice({
    name: "navigation",
    initialState,
    reducers: {
        saveNavMenuItems: (state, action) => {
            const fetchedMenuItems: MenuItemsResponse[] = action.payload;
            const processedMenuItems: MenuItemModel[] = [] as MenuItemModel[];
            
            const modulesList: Set<string> = new Set<string>();
            fetchedMenuItems.forEach(item => {
                modulesList.add(item.serviceName);
            });

            modulesList.forEach(module => {

                const newModule: MenuItemModel = { children: [] as MenuItemModel[] } as MenuItemModel;

                newModule.label = module;
                newModule.key = module.toLowerCase();

                fetchedMenuItems.forEach(item => {

                    if(item.serviceName === module) {

                        const newItem: MenuItemModel = {} as MenuItemModel;

                        newItem.icon = item.icon;
                        newItem.url = item.url;
                        newItem.label = item.name;
                        newItem.key = item.id.toString();

                        newModule.children?.push(newItem);
                    }
                });

                processedMenuItems.push(newModule);
            })

            persistMenuItems(processedMenuItems);
            state.menuItems = processedMenuItems;
        }
    }
});

export const { saveNavMenuItems } = navigationSlice.actions;
export default navigationSlice.reducer;
