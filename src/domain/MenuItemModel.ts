export type MenuItemModel = {
    label: string,
    key: string;
    url?: string;
    children?: MenuItemModel[];
    type?: string,
    icon?: string;
}