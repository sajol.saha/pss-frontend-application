import { BaseCol } from '@app/components/common/BaseCol/BaseCol';
import { BaseRow } from '@app/components/common/BaseRow/BaseRow';
import { UserGroupMappingForm } from '@app/components/user/userGroupMapping/UserGroupMappingForm';
import { UserGroupMappingTable } from '@app/components/user/userGroupMapping/UserGroupMappingTable';

export const UserGroupMappingPage: React.FC = () => {
  return (
    <BaseRow gutter={[0, 30]}>
      <BaseCol xs={24}>
        <UserGroupMappingForm />
      </BaseCol>
      <BaseCol xs={24}>
        <UserGroupMappingTable />
      </BaseCol>
    </BaseRow>
  );
};
