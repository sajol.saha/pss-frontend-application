import { apiSlice } from '@app/services/api/apiSlice';

export const roleApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    // endpoints here
    getItems: builder.query({
      query: () => ({
        url: '/roles',
      }),
      providesTags: ['api'],
    }),
    createItem: builder.mutation({
      query: (data) => ({
        url: '/roles',
        method: 'POST',
        body: data,
      }),
      invalidatesTags: ['api'],
    }),

    updateItem: builder.mutation({
      query: ({ id, data }) => ({
        url: `/roles/${id}`,
        method: 'PUT',
        body: data,
      }),
      invalidatesTags: ['api'],
    }),
    deleteItem: builder.mutation({
      query: (id) => ({
        url: `/roles/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['api'],
    }),
    getItem: builder.query({
      query: (id) => `/roles/${id}`,
    }),
  }),
});


export const {
  useCreateItemMutation,
  useGetItemsQuery,
  useUpdateItemMutation,
  useDeleteItemMutation,
  useGetItemQuery,
} = roleApi;
