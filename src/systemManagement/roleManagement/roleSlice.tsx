import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  roleData: null,
};

const roleSlice = createSlice({
  name: 'userRole',
  initialState,
  reducers: {
    saveRowData: (state, action) => {
      state.roleData = action.payload;
    },
    resetRowData: (state) => {
      state.roleData = initialState.roleData;
    },
  },
});

export const { saveRowData, resetRowData } = roleSlice.actions;

export default roleSlice.reducer;
