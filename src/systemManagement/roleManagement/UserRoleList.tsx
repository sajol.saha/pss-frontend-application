import { EditOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { Loading } from '@app/components/common/Loading/Loading';
import { notificationController } from '@app/controllers/notificationController';
import ServerErrorPage from '@app/pages/ServerErrorPage';
import { Button, Popconfirm, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useDeleteItemMutation, useGetItemsQuery } from './roleApi';
import { resetRowData, saveRowData } from './roleSlice';

interface DataType {
  key: string;
  name: string;
  description: string;
  status: boolean;
  id: number;
}
interface UserRoleListFromProps {
  visible: boolean;
  onConfirm: () => void;
  onCancel: () => void;
  handleVisible: () => void;
}

const UserRoleList: React.FC<UserRoleListFromProps> = ({ handleVisible }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  // For get saveRowData from roleSlice
  const { roleData } = useSelector((state: any) => state.role || null);
  // For Data fetching from RTK query
  const { data: items, isError, isLoading } = useGetItemsQuery({});
  // For deleteItem mutation
  const [deleteItem] = useDeleteItemMutation();

  if (isError) {
    console.log({ isError });
    return (
      <div>
        <ServerErrorPage />
      </div>
    );
  }
  if (isLoading) {
    return (
      <div>
        <Loading />{' '}
      </div>
    );
  }
  // handle delete method
  const handleDelete = async (id: number) => {
    try {
      await deleteItem(id);
      dispatch(resetRowData());
    } catch (error: any) {
      console.log(error.message);
    }
  };
  // For popConfirm method
  const confirm = (id: number) => {
    handleDelete(id);
    notificationController.success({
      message: t('notifications.success'),
    });
  };
  // For popConfirm cancel method
  const cancel = () => {
    notificationController.success({
      message: t('common.cancel'),
    });
  };

  const columns: ColumnsType<DataType> = [
    {
      title: t('tables.roleTitle'),
      dataIndex: 'name',
      key: 'name',
      // render: (text) => <a>{text}</a>,
    },
    {
      title: t('tables.roleDescription'),
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: t('tables.roleAction'),
      key: 'action',
      render: (_, record: any) => (
        <Space size="middle">
          <Button
            type="primary"
            onClick={() => {
              handleVisible();
              roleData && roleData?.record ? dispatch(resetRowData()) : dispatch(saveRowData(record));
            }}
          >
            {t('tables.roleUpdate')} <EditOutlined style={{ fontSize: '25px', color: '#08c' }} />
          </Button>
          <Popconfirm
            title={t('popconfirm.content')}
            onConfirm={() => confirm(record.id)}
            onCancel={cancel}
            okText={t('popconfirm.yes')}
            cancelText={t('popconfirm.no')}
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Table columns={columns} dataSource={items} style={{ marginTop: '1rem' }} rowKey="id" />
    </>
  );
};

export default UserRoleList;
