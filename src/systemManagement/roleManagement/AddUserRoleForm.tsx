import { notificationController } from '@app/controllers/notificationController';
import { Button, Form, Input, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import UserRoleList from './UserRoleList';
import { useCreateItemMutation, useUpdateItemMutation } from './roleApi';
import { resetRowData, saveRowData } from './roleSlice';

interface Values {
  id: number;
  name: string;
  description: string;
}

interface CollectionCreateFormProps {
  visible: boolean;
  onCreate: (values: Values) => void;
  onCancel: () => void;
  handleVisible: () => void;
}

const CollectionCreateForm: React.FC<CollectionCreateFormProps> = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  // For get saveRowData from roleSlice
  const { roleData } = useSelector((state: any) => state.role || null);
  //For updateItem mutation
  const [updateItem, { isSuccess }] = useUpdateItemMutation();
  // for set update modal initialValues
  useEffect(() => {
    if (roleData) {
      form.setFieldsValue({
        name: roleData.name || '',
        description: roleData.description || '',
      });
    }
  }, [form, roleData]);
  // For set message for update item
  useEffect(() => {
    if (isSuccess) {
      notificationController.success({
        message: t('notifications.success'),
      });
    }
  }, [isSuccess, t]);
  return (
    <Modal
      open={visible}
      title={roleData ? t('tables.updateUserRole') : t('tables.addUserRole')}
      okText={roleData ? t('tables.roleUpdate') : t('tables.roleCreate')}
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values: Values) => {
            form.resetFields();
            // Proceed with create or update action
            roleData ? updateItem({ id: roleData.id, data: values }) : onCreate(values);
            dispatch(resetRowData());
            onCancel();
          })
          .catch((error: any) => {
            console.log('Validate Failed:', error);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item name="name" label="Role Name" rules={[{ required: true, message: t('tables.statusRoleMessage') }]}>
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input.TextArea />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const AddUserRoleForm: React.FC = () => {
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  // For createItem mutation
  const [createItem, { isSuccess }] = useCreateItemMutation();
  // For create data
  // for success
  useEffect(() => {
    if (isSuccess) {
      notificationController.success({
        message: t('notifications.success'),
      });
    }
  }, [isSuccess, t]);

  const onCreate = async (values: Values) => {
    await createItem({ ...values });
    handleVisible();
    dispatch(resetRowData());
  };
  // For Handle Visible method
  const handleVisible = () => {
    setVisible(!visible);
  };

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          handleVisible();
          dispatch(resetRowData());
        }}
      >
        {t('tables.addUserRole')}
      </Button>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          dispatch(saveRowData({ id: null, name: null, description: null }));
          handleVisible();
        }}
        handleVisible={function (): void {
          throw new Error('Function not implemented.');
        }}
      />
      <>
        <UserRoleList
          handleVisible={handleVisible}
          onConfirm={function (): void {
            throw new Error('Function not implemented.');
          }}
          onCancel={function (): void {
            throw new Error('Function not implemented.');
          }}
          visible={false}
        />
      </>
    </>
  );
};

export default AddUserRoleForm;
