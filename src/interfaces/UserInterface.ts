export interface UserType {
  id: number;
  groupId?: number;
  name: string;
  groupName?: string[];
}
