import { getAll } from '@app/api/group.api';
import { customApiBaseUrl } from '@app/api/http.api';
import * as S from '@app/pages/uiComponentsPages//UIComponentsPage.styles';
import { readToken } from '@app/services/localStorage.service';
import { Button, Form, Input, Popconfirm, Select, Space, Table, notification } from 'antd';
import { FormInstance } from 'antd/lib/form';
import axios from 'axios';
import { useEffect, useState } from 'react';

const API_BASE_URL = customApiBaseUrl;
const { Option } = Select;

type Props = {};

interface GroupRowData {
  id: number;
  name: string;
}

const UserGroup = (props: Props) => {
  const [form] = Form.useForm<FormInstance>();
  const [tableData, setTableData] = useState<GroupRowData[]>([]);
  const [groupInput, setGroupInput] = useState('');
  const [editingRow, setEditingRow] = useState<GroupRowData | null>(null);

  const columns = [
    {
      title: 'Group Name',
      dataIndex: 'name',
      id: 'name',
    },
    {
      title: 'Actions',
      id: 'actions',
      render: (_, row: GroupRowData) => (
        <Space>
          <Button type="link" onClick={() => handleEdit(row)}>
            Edit
          </Button>
          <Popconfirm title="Are you sure you want to delete this row?" onConfirm={() => handleDelete(row.id)}>
            <Button type="link">Delete</Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    getAll().then((response) => setTableData(response));
  }, []);

  const handleGroupInput = (e: any) => {
    setGroupInput(e.target.value);
  };

  const onFinish = (values: GroupRowData) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${readToken()}`,
      },
      timeout: 5000,
    };

    if (editingRow) {
      axios
        .put(`${API_BASE_URL}/groups/${editingRow.id}`, { name: groupInput }, config)
        .then((response) => {})
        .catch((error) => {
          notification.error({
            message: error.response.data.code,
            description: error.response.data.message,
          });
        });
    } else {
      axios
        .post(`${API_BASE_URL}/groups`, { name: groupInput }, config)
        .then((response) => {
          form.resetFields();
        })
        .catch((error) => {
          notification.error({
            message: error.response.data.code,
            description: error.response.data.message,
          });
        });
    }
    form.resetFields();
  };

  const handleEdit = (row: GroupRowData) => {
    form.setFieldsValue(row);
    setEditingRow(row);
  };

  const handleDelete = (id: number) => {
    const filteredData = tableData.filter((row) => row.id !== id);
    setTableData(filteredData);
  };

  return (
    <>
      <S.Card title={'GROUP FORM'}>
        <Form form={form} onFinish={onFinish}>
          <div>
            <Form.Item name="name" label="Group Name">
              <Input value={groupInput} onChange={handleGroupInput} />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                {editingRow ? 'Update' : 'Add to Table'}
              </Button>
            </Form.Item>
          </div>
        </Form>
        <Table style={{ width: '100%' }} dataSource={tableData} columns={columns} />
      </S.Card>
    </>
  );
};

export default UserGroup;
