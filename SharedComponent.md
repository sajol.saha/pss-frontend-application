# Shared Components and Hooks

## Components

### **1. UI Components:**

- Button
- Input
- Checkbox
- Radio button
- Dropdown
- Modal
- Tooltip
- Alert
- Avatar
- Progress bar
- Card
- Badge
- Pagination
- Accordion
- Date picker

### **2. Form Components:**

- Form input
- Form textarea
- Form select
- Form checkbox group
- Form radio button group
- Form date picker
- Form file uploader
- Form validation error message

### **3. Layout Components:**

- Navbar
- Sidebar
- Header
- Footer
- Container
- Grid system
- Breadcrumbs
- Tabs
- Accordion
- Stepper

### **4. Data Visualization Components:**

- Line chart
- Bar chart
- Pie chart
- Area chart
- Scatter plot
- Heatmap
- Table
- Data grid
- Tree view
- Map component
- Gauge chart

### **5. Utility Components:**

- Loading spinner
- Error boundary
- Toast notification
- Modal dialog
- Confirmation dialog
- Tooltip
- Avatar
- Badge
- Dropdown menu
- Progress bar
- Responsive image

### **6. Navigation Components:**

- Tabs
- Navigation bar
- Sidebar menu
- Breadcrumbs
- Pagination
- Stepper
- Accordion menu
- Dropdown menu

### **7. Authentication Components:**

- Login form
- Registration form
- Forgot password form
- Reset password form
- Profile settings form
- Account activation form

### **8. Media Components:**

- Image gallery
- Video player
- Audio player
- Carousel
- Lightbox

### **9. Notification Components:**

- Toast notifications
- Alert messages
- Snackbar
- Notification badges

### **10. Data Display Components:**

- List view
- Card view
- Table view
- Tree view
- Tag cloud
- Calendar view
- Timeline
- Data filters

<br/>

## Shared Hooks

### **1. State Management Hooks:**

- useState
- useEffect
- useContext
- useReducer

### **2. Form Handling Hooks:**

- useForm
- useField
- useFormValidation

### **3. Network Request Hooks:**

- useAxios
- useFetch
- useSWR (for data fetching and caching)

### **4. Authentication Hooks:**

- useAuth (for handling authentication state)
- useAuthRedirect (for redirecting unauthenticated users)

### **5. Animation Hooks:**

- useAnimation
- useScroll
- useSpring

### **6. Media Hooks:**

- useMediaQuery (for handling responsive design)
- useImageLazyLoad (for lazy loading images)

### **7. Routing Hooks:**

- useHistory (for accessing and manipulating browser history)
- useParams (for accessing route parameters)
- useRouteMatch (for matching the current URL)

### **8. Localization Hooks:**

- useTranslation (for internationalization and translation)

### **9. Custom Hooks:**

- useReduxHooks
- UseAutoNightMode
- UseDebounce
- UseLanguage
- UseMounted
- UseResponsive
- useThemeWatcher
- useErrorHandler
